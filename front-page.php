<?php
/**
 * The main template for a static front page
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */

get_header(); ?>

<div class="owl-carousel owl-theme">
		<div>
			<figure class="orbit-figure">
				<img src="http://www.staging.jordiradstake.nl/marliesdinnissen.nl/wp-content/uploads/revslider/furniture/28164947_2097284130502188_3847993297672444331_o.jpg" alt="Space">
				<figcaption class="orbit-caption">Space, the final frontier.</figcaption>
			</figure>
		</div>
		<div>
			<figure>
				<img class="orbit-image" src="http://www.staging.jordiradstake.nl/marliesdinnissen.nl/wp-content/uploads/revslider/furniture/28164947_2097284130502188_3847993297672444331_o.jpg" alt="Space">
				<figcaption class="orbit-caption">Space, the final frontier.</figcaption>
			</figure>		</div>
</div>



	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php inti_hook_grid_open(); ?>

				<?php inti_hook_inner_content_before(); ?>

				<header class="archive-header">
					<?php
					/**
					 * Get the page loop
					 * Content of the page that is set as the front-page is
					 * displayed as a kind of header to the rest of
					 * what is displayed later displayed...
					 */
					get_template_part('loops/loop', 'page'); ?>
				</header><!-- .archive-header -->

				<?php
				/**
				 * Get the main loop
				 * ...by default we also place the loop of posts
				 * under the front-page content. Modify as needed or
				 * overwrite front-page.php with a child theme version.
				 */
			//	get_template_part('loops/loop', 'frontpage'); ?>

			<hr class="style12">

			<?php echo do_shortcode('[recent_products per_page="10" columns="5" orderby="date" order="desc"]'); ?>

			<hr class="style12">

				<?php framework_do_page_header_background_img(); ?>

				<?php inti_hook_inner_content_after(); ?>

				<div class="grid-container fluid">
					<div class="grid-x grid-margin-x">
						<div class="cell large-12"
							<?php dynamic_sidebar('sidebar-front-page-footer'); ?>
						</div>
					</div>
				</div>

			<?php inti_hook_grid_close(); ?>

		</div><!-- #content -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->


<?php get_footer(); ?>
