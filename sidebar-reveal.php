<?php
/**
 * The sidebar template for the reveal widget area
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */?>

<div id="site-reveal-sidebar" class="sidebar">
	<div class="grid-x grid-margin-x grid-margin-y align-center">
		<?php dynamic_sidebar('sidebar-reveal'); ?>
	</div>
</div>
