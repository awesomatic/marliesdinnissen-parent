<?php
/**
 * WooCommerce Theming snippets
 * WooCommerce codex
 *
 * @package Inti
 * @since 1.0.0
 * @link https://docs.woocommerce.com/documentation/plugins/woocommerce/woocommerce-codex/snippets/snippets-theming
 */

 // Remove each style one by one
 add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
 function jk_dequeue_styles( $enqueue_styles ) {
 	//unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
 	//unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
 	//unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
 	return $enqueue_styles;
 }

// https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)

// Theme support

 add_theme_support( 'wc-product-gallery-zoom' );
 add_theme_support( 'wc-product-gallery-lightbox' );
 add_theme_support( 'wc-product-gallery-slider' );

 add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );

 function iconic_cart_count_fragments( $fragments ) {

     $fragments['span.header-cart-count'] = '<span class="header-cart-count in-cart">' . WC()->cart->get_cart_contents_count() . '</span>';

     return $fragments;

 }


 add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product)
{
    return '<span class="onsale">Sale</span>';
  }


  /* Plugin: Themify filter */

  //add_action('woocommerce_before_shop_loop', 'themify_filter', 5);

  function themify_filter() {

    echo do_shortcode( '[searchandfilter taxonomies="category,post_tag"]' );

  	//echo do_shortcode('[searchandfilter id="categorie"]');

  }



/**
 * Remove the breadcrumbs
 */
//add_action( 'init', 'woo_remove_wc_breadcrumbs' );
function woo_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}
