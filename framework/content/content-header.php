<?php
/**
 * Content - Header, Site Banner, Menus, Off-canvas
 * add content to predefined hooks
 * found throughout the theme
 *
 * @package Inti
 * @since 1.0.0
 * @license GNU General Public License v2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
 */


/**
 * Head goes in the head
 * Add our <head> template into the <head>
 *
 * @since 1.0.0
 */
function inti_do_inti_head() {
	get_template_part('template-parts/part-head');
}
add_action('wp_head', 'inti_do_inti_head', 1);


/**
 * Custom JS
 * Add custom JS from theme options into the <head>
 *
 * @since 1.0.0
 */
function inti_do_head_js() {
	$customjs = stripslashes(get_inti_option('head_js', 'inti_headernav_options'));
	if ( $customjs ) { ?>
		<!-- Custom JS -->
		<script>
			<?php echo $customjs; ?>
		</script>
		<!-- End Custom JS -->
<?php
	}
}
add_action('inti_hook_head', 'inti_do_head_js', 1);


/**
 * Custom CSS
 * Add custom CSS from theme options into the <head>
 *
 * @since 1.0.0
 */
function inti_do_head_css() {
	$customcss = stripslashes(get_inti_option('head_css', 'inti_headernav_options'));
	if ( $customcss ) { ?>
		<!-- Custom CSS -->
		<style>
			<?php echo $customcss; ?>
		</style>
		<!-- End Custom CSS -->
<?php
	}
}
add_action('inti_hook_head', 'inti_do_head_css', 1);


/**
 * Custom Meta Tags
 * Add custom meta tags from theme options into the <head>
 *
 * @since 1.0.0
 */
function inti_do_head_meta() {
	$custommeta = stripslashes(get_inti_option('head_meta', 'inti_headernav_options'));
	if ( $custommeta ) { ?>
		<!-- Custom Meta Tags -->
		<?php echo $custommeta; ?>
		<!-- End Custom Meta Tags -->
<?php
	}
}
add_action('inti_hook_head', 'inti_do_head_meta', 1);


/**
 * Custom Code first thing inside <body>
 *
 * @since 1.0.0
 */
function inti_do_body_inside() {
	$custombodyinside = get_inti_option('body_inside', 'inti_headernav_options');
	if ( $custombodyinside ) { ?>
		<?php echo trim($custombodyinside); ?>
<?php
	}
}
add_action('inti_hook_site_before', 'inti_do_body_inside', 1);


/**
 * Add opening wrappers for default off-canvas menu behaviour
 *
 * @since 1.0.0
 */
function inti_do_site_off_canvas_header() {
	?>
	<div class="off-canvas-menu off-canvas position-right" id="inti-off-canvas-menu" data-off-canvas>
		<?php inti_hook_off_canvas(); ?>
	</div>
	<div class="off-canvas-content" data-off-canvas-content>
		<?php inti_hook_off_canvas_content(); ?>
	<?php
}
add_action('inti_hook_site_before', 'inti_do_site_off_canvas_header');


/**
 * Add closing wrappers for default off-canvas menu behaviour
 *
 * @since 1.0.0
 */
function inti_do_site_off_canvas_footer() {
	?>
	</div><!-- .off-canvas-wrapper-content -->

	<?php
}
add_action('inti_hook_site_after', 'inti_do_site_off_canvas_footer');


/**
 * Add main menu before or after site banner
 *
 * @since 1.0.0
 */
function inti_do_main_dropdown_menu() {
   //adds the main menu
	if ( has_nav_menu('dropdown-menu') ) {?>
		<div id="site-banner-sticky-container" class="sticky-container" data-sticky-container>
			<div class="sticky-off" data-sticky data-options="marginTop:0;" style="width:100%">
				<div class="site-top-bar-container">
					<nav class="top-bar" id="top-bar-menu">



					<?php

					/**
					* Add logo or site title to the navigation bar
					* i. if one is set for the 'mobile nav' in customizer
					* ii. if a different one is to be shown on the nav bar if it's currently sticky
					*/
					$mobile_logo = get_inti_option('show_nav_logo_title', 'inti_customizer_options', 'none');
					$sticky_logo = get_inti_option('show_sticky_logo_title', 'inti_customizer_options', 'none');
					/**
					* Add logo or site title to the site-banner, hidden in on smaller screens where another logo is shown on top-bar
					*/
					$logo = get_inti_option('logo_image', 'inti_customizer_options', 'none'); ?>

					<?php if ( $logo ) : ?>
						<div class="top-bar-left show-for-mlarge not-stuck">
							<div class="site-logo">
								<?php if ( get_inti_option('logo_image', 'inti_customizer_options') ) : ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
										<?php inti_do_srcset_image(get_inti_option('logo_image', 'inti_customizer_options'), esc_attr( get_bloginfo('name', 'display') . ' logo')); ?>
									</a>
								<?php endif; ?>
							</div>
							<div class="site-title"><?php bloginfo('name'); ?></div>
						</div>
					<?php endif; ?>

					<?php // logo in nav on small screens
					if ('none' != $mobile_logo) : ?>
						<div class="top-bar-left hide-for-mlarge mobile-logo">
							<div class="site-logo">
								<?php if ( get_inti_option('nav_logo_image', 'inti_customizer_options') ) : ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
										<?php inti_do_srcset_image(get_inti_option('nav_logo_image', 'inti_customizer_options'), esc_attr( get_bloginfo('name', 'display') . ' logo')); ?>
									</a>
								<?php endif; ?>
							</div>
							<div class="site-title"><?php bloginfo('name'); ?></div>
						</div>

					<?php endif;

					// logo on nav when sticky (needs CSS _navigation.scss)
					if ('none' != $sticky_logo) : ?>
						<div class="top-bar-left show-for-mlarge sticky-logo animated fadeInLeft">
							<div class="site-logo">
								<?php if ( get_inti_option('nav_logo_image', 'inti_customizer_options') ) : ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
										<?php inti_do_srcset_image(get_inti_option('nav_logo_image', 'inti_customizer_options'), esc_attr( get_bloginfo('name', 'display') . ' logo')); ?>
									</a>
								<?php endif; ?>
							</div>
							<div class="site-title"><?php bloginfo('name'); ?></div>
						</div>

					<?php endif; ?>


						<div class="top-bar-left show-for-large main-dropdown-menu">
							<?php echo inti_get_dropdown_menu(); ?>
						</div>

						<!-- <div class="top-bar-right show-for-mlarge">
							<?php $showsocial = get_inti_option('nav_social', 'inti_headernav_options');
								if ($showsocial) echo inti_get_dropdown_social_links();?>
						</div> -->
						<div class="top-bar-right show-for-small">
							<ul class="menu">
								<li style="margin-top:2px;"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="cart-contents"><img width="18" src="<?php echo get_stylesheet_directory_uri(); ?>/library/src/img/user-icon.png" alt="Mijn account"></a></li>
								<li><a class="cart-contents" data-toggle="exampleModal8" data-animate="slide-in-down slide-out-up" title="<?php _e( 'View your shopping cart' ); ?>"><img width="16" src="<?php echo get_stylesheet_directory_uri(); ?>/library/src/img/cart-icon.png" alt="Bekijk winkelmandje">
 <span class="header-cart-count"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></a>
</li>
							</ul>
						</div>

						<div class="top-bar-right">
								<div class="full reveal" id="exampleModal8" data-reveal>
									<br><br><br><br>
									<div class="grid-container fluid">
										<div class="grid-x grid-margin-x">
											<div class="small-12 cell">
												<?php get_sidebar('reveal'); ?>
											</div><!-- .cell -->
										</div><!-- .grid-x -->
									</div>
								  <button class="close-button" data-close aria-label="Close reveal" type="button">
								    <span aria-hidden="true">&times;</span>
								  </button>
								</div>
						</div>

						<div class="top-bar-right hide-for-large">
							<ul class="menu">
								<li class="menu-text off-canvas-button"><a data-toggle="inti-off-canvas-menu">
									<div class="hamburger">
										<span></span>
										<span></span>
										<span></span>
									</div>
								</a></li>
							</ul>
						</div>


					</nav>
				</div>
			</div>
		</div>
	<?php
	}
}
add_action('inti_hook_site_banner_after', 'inti_do_main_dropdown_menu');




/**
 * Add main offcanvas menu
 *
 * @since 1.0.0
 */
function inti_do_main_off_canvas_menu() {
   //adds the main menu
	if ( has_nav_menu('off-canvas-menu') ) {
		echo inti_get_drilldown_menu();
		$showsocial = get_inti_option('nav_social', 'inti_headernav_options');
		if ($showsocial) echo inti_get_off_canvas_social_links();
	}
}
add_action('inti_hook_off_canvas', 'inti_do_main_off_canvas_menu');





/**
 * Featured img on pages
 *
 * @since 1.0.0
 */
 function wpse207895_featured_image() {
     //Execute if singular
     if ( is_singular() ) {

         $id = get_queried_object_id ();

         // Check if the post/page has featured image
         if ( has_post_thumbnail( $id ) ) {

             // Change thumbnail size, but I guess full is what you'll need
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'full' );

             $url = $image[0];

         } else {

             //Set a default image if Featured Image isn't set
             $url = '';

         }
     }

     return $url;
 }


/**
* Background img in page header
*
* @since 1.0.0
*/

add_action('inti_hook_content_before', 'framework_do_page_header_background_img', 1);

function framework_do_page_header_background_img() { }

/**
 * Preloader
 *
 * @since 1.0.0
 */

 add_action('inti_hook_site_before', 'framework_do_preloader', 1);

 function framework_do_preloader() { ?>

		<div class="preloader-wrapper">
	 		<div class="preloader">
	 			<!-- <i class="fas fa-spinner fa-pulse fa-3x"></i> -->
				<img src="http://marliesdinnissen.nl/wp-content/uploads/2018/05/logo-white.png" width="75" alt="Site wordt geladen ..">
	 	 	</div>
	 	</div>

<?php }


 function framework_orbit_slider_front() { ?>

	 <!-- <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
	   <div class="orbit-wrapper">
	     <div class="orbit-controls">
	       <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
	       <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
	     </div>
	     <ul class="orbit-container">
	       <li class="is-active orbit-slide">
	         <figure class="orbit-figure">
	           <img class="orbit-image" src="https://placehold.it/1200x600/999?text=Slide-1" alt="Space">
	           <figcaption class="orbit-caption">Space, the final frontier.</figcaption>
	         </figure>
	       </li>
	       <li class="orbit-slide">
	         <figure class="orbit-figure">
	           <img class="orbit-image" src="https://placehold.it/1200x600/888?text=Slide-2" alt="Space">
	           <figcaption class="orbit-caption">Lets Rocket!</figcaption>
	         </figure>
	       </li>
	       <li class="orbit-slide">
	         <figure class="orbit-figure">
	           <img class="orbit-image" src="https://placehold.it/1200x600/777?text=Slide-3" alt="Space">
	           <figcaption class="orbit-caption">Encapsulating</figcaption>
	         </figure>
	       </li>
	       <li class="orbit-slide">
	         <figure class="orbit-figure">
	           <img class="orbit-image" src="https://placehold.it/1200x600/666&text=Slide-4" alt="Space">
	           <figcaption class="orbit-caption">Outta This World</figcaption>
	         </figure>
	       </li>
	     </ul>
	   </div>
	   <nav class="orbit-bullets">
	     <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
	     <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
	     <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
	     <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
	   </nav>
	 </div> -->

 <?php }
