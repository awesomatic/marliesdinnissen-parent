<?php
/**
 * The template for displaying the gallery post format
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */

/**
 * In the Theme Options, users can choose whether post archives display the full posts
 * or a group of excerpts with a "read more" button to see the rest. Post formats should
 * check which option is set and modify the interface accordingly.
 * 1 == standard (shown on singles or on archives when option is set to 1)
 * 2 == short (shown on archives when option is set to 2)
 *
 */
$interface = get_inti_option('blog_interface', 'inti_general_options');

if ($interface == 1 || is_single()) : // standard interface
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-body">

			<?php inti_hook_post_header_before(); ?>

			<header class="entry-header">
				<?php inti_hook_post_header(); ?>
			</header><!-- .entry-header -->

			<?php inti_hook_post_header_after(); ?>

			<div class="callout">
				<div class="entry-content">
					<?php inti_hook_post_content_before_the_content(); ?>
					<?php the_content(); ?>
					<?php inti_hook_post_content_after_the_content(); ?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php inti_hook_post_footer(); ?>
				</footer><!-- .entry-footer -->
			</div>

		</div><!-- .entry-body -->
	</article><!-- #post -->

<?php
else : // short interface with excerpt ?>

<?php $term_list = wp_get_post_terms($post->ID, array('gallery-category', 'gallery-tag'), array("fields" => "all")); ?>

<div class="masonry-css-item column filterDiv <?php foreach($term_list as $term_single ) { echo $term_single->slug . " "; } //do something here ?>">

	<?php //foreach($term_list as $term_single ) { echo $term_single->slug . " "; } //do something here ?>

	<article id="post-<?php the_ID(); ?>">
		<div class="entry-body">

			<?php inti_hook_post_header_before(); ?>

			<header class="entry-header">
				<?php// inti_hook_post_header(); ?>
			</header>

			<?php inti_hook_post_header_after(); ?>

			<div class="container">
				<img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('full'); } ?>" alt="Avatar" class="image">
			  <a href="<?php the_permalink(); ?>">
					<div class="overlay">
					<div>
							<div class="gallery-text text-center">
								<h2 style="color: #fff;" class="hide-for-small-only"><?php the_title(); ?></h2>
								<p>Bekijk <span class="hide-for-small-only">gallery </span><i class="fas fa-chevron-circle-right"></i></p>
							</div>
					</div>
			  </div>
				</a>
			</div>

				<footer class="entry-footer">

					<?php inti_hook_post_footer(); ?>

				</footer>

		</div><!-- .entry-body -->
	</article><!-- #post -->
</div>

<?php endif; ?>
