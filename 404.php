<?php
/**
 * The main template for pages
 *
 * @package Inti
 * @subpackage Templates
 * @since 1.0.0
 */


get_header(); ?>

<div class="hero-section static" style="background-color: #747748;">

	<div class="hero-section-text">

		<h2>Oeps, deze pagina bestaat niet (meer).</h2>

	</div>

</div>

	<div id="primary" class="site-content">

		<?php inti_hook_content_before(); ?>

		<div id="content" role="main" class="<?php apply_filters('inti_filter_content_classes', ''); ?>">

			<?php inti_hook_grid_open(); ?>

				<?php inti_hook_inner_content_before(); ?>

				<?php // get the main loop
				get_template_part('template-parts/part', '404'); ?>

				<?php inti_hook_inner_content_after(); ?>

			<?php inti_hook_grid_close(); ?>

		</div><!-- #content -->

		<?php inti_hook_content_after(); ?>

	</div><!-- #primary -->


<?php get_footer(); ?>
